# Cours pour CESS
Ce git contient la liste de toutes mes synthèses, les liens, les examens blancs et les programmes pour les examens.

## Liens
Voici quelques liens qui sont intéressant à accéder rapidement:

* [Portail CESS Général](http://enseignement.be/index.php?page=27250&navi=3740)

## License
Le contenu de ce git est sous license CC-SA (Creative Commons Share-Alike).
