# Chimie
## Modèle de lewis et autres
* Formule brute :

```
C4NH11
```

* Formule développée (lewis) :
![Formule déveoppée](https://media.kartable.fr/uploads/finalImages/final_5fb4eca027beb8.52961974.png)

* Formule semi-développée :

```
CH3 --- CH2 --- CH2 --- CH2 --- NH2
```

* Modèle de Bohr :

![Modèle de Bohr, wikimedia](https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/Atome_bohr_couches_electroniques_KLM.svg/800px-Atome_bohr_couches_electroniques_KLM.svg.png)
*Les électrons qui sont sur la dernière couche sont les électrons de valence, ceux qui peuvent faire des liaisons chimiques.*

## Types de liaisons
Trouver le delta de l'électro négativité : électronégativité du premier élément - électronégativité d'un deuxième

Pour connaitre le type de liaison, il faut utiliser le barème suivant avec le delta de l'électronégativité

```
0          0.5        1.7        2          ...
|__________|__________|__________|__________|
 covalente  covalente    ↓ ↓ ↓     ionique
 non-polar  polarisée
```

* Entre 0 et 0.5, la liaison est covalente non-polaire, aussi appellé liaison "pure"
* Entre 0.5 et 1.7, la liaison est covalente polarisée 
* Entre 1.7 et 2, si c'est une liaison entre 2 métaux, la liaison est ionique, sinon la liaison est covalente polarisée 
* Plus de 2, la liaison est ionique

## Réactions


