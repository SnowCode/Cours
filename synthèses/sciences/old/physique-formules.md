Ceci est la listes dees formules à connaître pour le cours de physique.

## Les ondes
### Longueur d'onde

| Symbole       | Signification                     | Unité     | Domaine       |
| ------------- | -------------------------         | --------- | -------       |
| $\lambda$     | Longueur d'onde                   | $m$       | Général       |
| $c$           | Célérité                          | $m/s$     | Général       |
| $T$           | Période                           | $s$       | Général       |
| $f$           | Fréquence                         | $Hertz$   | Général       |
| $\Theta$      | Angle                             | $rad$     | Diffraction   |
| $a$           | Ouverture (diffraction)           | $m$       | Diffraction   |
| $L$           | Taille de la tache centrale       | $m$       | Diffraction   |
| $E$           | Energie                           | $J$       | Spectroscopie |
| $h$           | Constante de Plank                | $J/s$     | Spectroscopie |
| $t$		| Temps				    | $s$	| Général	|
| $d$		| Distance			    | $m$	| Général	| 
| $v$		| Vitesse			    | $m/s$	| Général	|

$$
\lambda = cT = \frac{c}{f}
$$

$$
f = \frac{1}{T}
$$

$$
\Theta = \frac{\lambda}{a} = \frac{L}{2D}
$$

$$
E = hf = \frac{hc}{\lambda}
$$

$$
v = \frac{d}{t}
$$

## Dynamique Newtoniennes (Séquence 4)
Champs de pesanteur

| Variable | Signification | Unité |
| --- | --- | --- | 
| $\vec{F}$ | Force | $N$ | 
| $\vec{F}_{A \rightarrow B}$ | ??? | $N$ |
| $E$ | Energie d'intéraction électrostatique | $V/m$ | 
| $d$ | Distance | $m$ | 
| $U_{AB}$ | Tension du dipole A vers le dipole B | $V$ |
| $q$ | Charge de la particule | $C$ | 

Force d'intéraction électrostatique

$$
\vec{F} = q\vec{E}
$$

Calculer l'énergie d'intéraction électrostatique

$$
E = \frac{U_{AB}}{d}
$$

Trouver $U_{AB}

$$
U_{AB} = \Delta V = V_2 - V_1
$$

