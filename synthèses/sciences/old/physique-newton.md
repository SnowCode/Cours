# Les lois de Newton

## Référentiel terrestre
Calcul d'une droite courte

## Référentiel héliocentrique
Mouvement circulaire long

## Référentiel Copernic

> Un référentiel galiléen, un objet est soumis à l'intertie 

1. Si la force des forces extérieur au système est nulle. Alors son centre de gravité est immobile ou en mouvement rectiligne.
2. La somme des vecteurs extérieurs exercés sur le système est égale à la dérivée du vecteur quantité de mouvement.
3. ↓^
Quantité de mouvement est notée $\vec{p}=m\vec{v}$ qui  



1. Bilan des forces
2. On projette 
