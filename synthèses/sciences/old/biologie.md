Savoirs

## Est que je sais définir les mots suivants : micro organisme pathogène, micro organisme non pathogène
Un **micro-organisme** est un organisme vivant invisible à l'oeil nu.

**Pathogène** est un adjectif qui signifie, qui peut provoquer des maladies ou endommager la biologie de l'hôte.

## Est que je sais définir les mots suivants : globules blancs, macrophages, lymphocytes T, lymphocytes B

Les **globules blancs** jouent un rôle clé dans la défense du corps contre les virus et les bactéries, qui peuvent causer des infections. C'est une catégorie de défence immunitaire (qui comprends les macrophages, les lymphocytes, etc).

Le **macrophage** est une grande cellule ayant la propriété d'ingérer et de détruire de n'importe quel corps étranger (cellules lésées ou vieillies, particules étrangères, bactéries) par phagocytose.

Les **lymphocytes T** détectent et neutralisent les cellules infectées par des virus. La réponse immunitaire spécifique est plus rapide et plus efficace pour des antigènes connus, c'est la mémoire immunitaire. La vaccination permet de former la mémoire immunitaire.

Les **lymphocytes B** sécrètent des anticorps, des molécules capables de reconnaître spécifiquement un antigène, alors que les lymphocytes T détruisent directement les cellules contaminées. Cependant, il arrive très souvent que les lymphocytes T et B collaborent ensemble dans la défense immunitaire de l'organisme.       

## Est que je sais décrire l’ensemble des étapes en détails de la réaction  immunitaire innée ?
Premièrement, un anti-gène doit traverser la barrière naturelle du corps. 

Ensuite, les **macrophages** vont phagocyter les **anti-gènes** et entraine une inflammation  (fièvre, rougeur, gonflement) qui va entrainer plus de globules blancs. 

La **cellule dentrétique** ainsi que les **lymphocytes**. Les **Lymphocytes T8 (tueurs)** vont tuer les cellules infectées. Les **LT helper mémoire et LB mémore** vont "enregistrer" l'infection ce qui permet de protéger le corps de manière plus efficace à la prochaine infection. Les **LB** vont produire des **anticorps** qui vont s'attacher à l'antigène pour le bloquer et rendre la phagocytose plus efficace, rapide et facile. Enfin, les **LT4 (helper)** vont sécrèter des molécules pour rendre la réction plus rapide.

Enfin, tout les globules blancs qui ont été utilisés se suicident, à l'exception des lymphocytes mémoires qui restent dans le corps.

## Est que je sais décrire l’ensemble des étapes en détails de la réaction immunitaire acquise ?
Premièrement, l'antigène doit traverser la barrière naturelle du corps.

Ensuite, les **macrophages** vont phagocyter les **anti-gènes** et entraine une inflammation  (fièvre, rougeur, gonflement) qui va entrainer plus de globules blancs. 

Contrairement à l'inné, les lymmphocytes vont directement être activer et produire des anticorps ce qui va rendre la réaction beaucoup plus rapide (grâce aux lymphocytes mémoires)

Enfin, tout les globules blancs qui ont été utilisés se suicident, à l'exception des lymphocytes mémoires qui restent dans le corps.

## Est que je sais décrire les étapes en détails de la phagocytose ?

Adhesion, ingestion, digestion, expution 

![Phagocytose](https://images.schoolmouv.fr/cycle4-svt-c27-img03.png)

## Est que je sais définir les mots suivants : antigène, anticorps

 Les **antigènes** sont apte à causer une reaction imunnitaire. Ils sont situés sur la parois des cellules (ou des virus).
 Les **anticorps** sont produit par les lymphocytes B, il s'accroche au pathogene dans le but de neutraliser l intru pour apres pouvoir etre digerer par les macrophages.

## Est que je sais donner la définition des mots suivants : greffe, vaccins

un vaccin va declencher une reaction imunitaire specifique, à l'aide du pathogene qui lui permettera de reconnaitre plus tard le pathogene avec une reaction qui sera plus rapide

prendre une partie d'un organisme vivant pour le transplanté sur un autre (une greffe peut etre animal ou vegetale)

## Est que je sais définir les mots suivants et les replaces sur le corps humain : système nerveux central, moelle épinière, encéphale, crâne et colonne vertébrale ?

Le rôle du système nerveux central, formé du cerveau et de la moelle épinière, est d'organiser, de contrôler et de réguler des fonctions essentielles de l'organisme comme la motricité, l'équilibre, la perception (sensibilité, vision, audition, odorat…)

La moelle épinière conduit les informations du cerveau vers les organes et, inversement, des organes vers le cerveau

L'encephale est l'ensemble des centres nerveux contenus dans le crâne (le cerveau, le tronc cerebrale et le cervelet ).

Le crane et la colonne vertebrale sont la pour proteger la moelle epiniere et le cerveau 

## Est que je sais donner les rôles du crâne et de la colonne vertébrale ?

Le crane et la colonne vertebrale sont la pour proteger la moelle epiniere et le cerveau 

## Est que je sais définir les mots suivants : liquide céphalo-rachidien, les meninges

Le liquide cephalo rachidien permet l'évacuation des déchets toxiques et le transport des hormones entre des régions éloignées du cerveau.

## Est que je saisis définir les mots suivants : système nerveux périphérique, nerfs crâniens et nerfs rachidiens

Le système nerveux périphérique (SNP) est un vaste réseau de nerfs qui permet de relier le système nerveux central au reste du corps et de véhiculer des informations sensitives et motrices.

## Est que je sais définir les mots suivants : homéostasie, récepteurs sensoriels, neurones, nerfs, substances psychotropes, influx nerveux, synapse, neurotransmetteurs

## Est que je connais l’ensemble des noms des étapes depuis la fécondation a l’accouchement

## Est que je sais décrire les caractères sexuels secondaires masculins et féminins ?

## Est que je sais décrire les étapes du cycle utérin et du cycle ovarien de la femme ?

## Est que je sais expliquer la relation entre les deux cycles ? (Cycle ovarien et utérin )

## Est que je sais définir les mots suivants : ménopause, contraception, contragestion, IVG et PMA ?

## Est que je sais quelles hormones sont impliquées dans la reproduction (masculine et féminine ) et comment la régulation se déroule-t-elle ?

Savoir faire et compétences

## Est que je sais interpréter des graphiques qui contiennent des données de dosage ?

## Est que je sais interpréter un tableau qui contient des donnees de dosage ?

## Est que je sais faire la différence entre une argumentation scientifique d’une croyance dans le cas des moyens de contraception ?

## Est que je sais expliquer l’influence de certaines substances sur le fonctionnement du système nerveux ?

## Est que je sais expliquer l’influence de certaines habitudes sur le fonctionnement du système nerveux ?

## Est que je sais réaliser un schéma fonctionnel de la régulation hormonale chez l’homme ?

## Est que je sais réaliser un schéma fonctionnel de la régulation hormonale chez la femme ?

## Est que je sais décrire les mécanismes qui permettent la transmission de la vie chez l’homme ?

## Est que je sais décrire de manière synthétique comment l’organisme est confronté en permanence à l’entrée des micro organismes ?

## Est que je sais expliquer comment les moyens de contraception permettent de contrôler la procréation ?
 
## Est que je sais justifier dans une situation concrète un comportement à adopter pour se protéger d’un risque infectieux ?
Le but est de contrer tous les moyens par lesquels un corps étranger peut entrer dans le corps, puis trouver des moyens pour faire l'inverse (exemple : masque, gel hydroalcholique, etc).

## Est que je sais expliquer l’impact de certaines substances sur la transmission synaptique à l’aide de diverses documents ?

## Est que je sais décrire l’organisation générale du système nerveux ?

## Est que je sais decrire les fonctions principales du système nerveux à partir d’une situation concrète ?

## Est que je sais réaliser un schéma du neurone et en déduire les caracteristiques ?

## Est que je sais expliquer le mécanisme de propagation de l’influx nerveux à travers la synapse ?

## Est que je sais aller chercher dans un document un ou des facteurs influençant le fonctionnement du système nerveux ?

## Est que je sais décrire le fonctionnement du testicule et la régulation hormonale qui lui est lié ?

## Est que je sais mettre en relation le cycle utérin et le cycle ovarien de la femme ?

## Est que je sais expliquer le mécanisme de régulation du cycle ovarien et cycle utérin ?

## Est que je sais décrire les étapes de la grossesse et son suivi ?

## Est que je sais comparer les données physiologiques d’une personne saine et d’une personne souffrant d’une maladie infectieuse ?

## Est que je sais identifier les modes de transmission de différents pathogenes à partir de cas concrets ?

## Est que je sais justifier l’importance des rappels de vaccination ?

## Est que je sais expliquer le mécanisme du rejet de greffe à partir de document ?

## Est que je sais identifier les facteurs qui peuvent perturber le sommeil sur base de documents ?

## Est que je sais comparer les différents mécanismes de différentes méthodes contraceptives ?


UAA5 : génétique
Savoirs

## Est que je sais définir les mots suivants : cellules, organites
Une cellule est l'unité vivante de base de tous les êtres vivants. 

Un organite est un "organe" de la cellule, ce sont les appareils qui permettent à la cellule de fonctionner. 

## Est que je sais expliquer les rôles des organites cellulaires ?
**Réticulum endopl

## Est que je sais decrire les étapes de la méiose ?

## Est que je sais définir les mots suivants : phénotype, génotype, biodiversité ?


+MM ## Est que je sais expliquer ce qu’est le code génétique ?

## Est que je sais définir les mots suivants : maladies génétiques, maladies chromosomiques ? Et la différence entre les deux ?

## Est que je sais définir les mots suivants et expliquer leurs rôles  : noyau, ADN, ARN, ribosomes, protéines ?

## Est que je sais définir les mots suivants : évolution, espèce, spéciation

## Est que je sais définir les mots suivants : sélection naturelle, brassage génétique, mutations, dérivé génétique ?

## Est que je sais définir les mots suivants : néodarwinisme, ancêtre commun, innovation évolutive ?

## Est que je sais retracer les étapes de la chronologie de l’évolution ?

## Est que je sais expliquer les origines de la vie

Savoir faire et compétences

## Est que je sais à partir de documents relever les informations concernant la génétique et les organiser de manière structurée ?

## Est que je sais à partir de documents relever les informations concernant l’évolution et les organiser de manière structurée ?

## Est que je sais expliquer la relation entre le phénotype, la structure des protéines et les séquences d’ADN ?

## Est que je sais formuler des hypothèses ?

## Est que je sais mettre en évidence dans un document les avantages des biotechnologies ?

## Est que je sais mettre en évidence dans un document les inconvénients des biotechnologies ?

## Est que je sais réaliser le schéma fonctionnel de la synthèse des protéines ?

## Est que je sais faire la différence entre une croyance et un modèle pour expliquer l’apparition de la vie sur terre ?

## Est que je sais faire la différence entre une croyance et un modèle pour expliquer l’évolution ?

## Est que je sais expliquer les différences entre une maladie génétique et une maladie chromosomique ?

## Est que je sais faire le lien entre la théorie de l’évolution et la classification moderne du vivant ?

## Est que je sais expliquer la relation entre gène et la structure primaire d’une protéine ?

## Est que je sais expliquer à partir de documents sur les applications en biotechnologies décrire l’impact sur le quotidien où l’environnement ?

## Est que je sais decrire les étapes de la synthèse des protéines (transcription et traduction) ?

## Est que je sais decrire les avantages et les inconvénients des OGM ?

## Est que je sais expliquer les conséquences possibles des mutations au niveau des cellules germinales ?

## Est que je sais analyser des documents sur l’apparition d’une nouvelle espèce pour mette en évidence les mécanismes d’apparitions ?

## Est que je sais decrire à l’aide de documents une application concrète en. Biotechnologie ?

## Est que je sais critiquer les arguelens émis par les théories de l’évolution (lamarckisme, fixisme, creationnisme) par rapport à la théorie néodarwinisme ?

## Est que je sais illustrer à partir d’un exemple que l’environnement peut modifier certains phénotypes ?

## Est que je sais interpréter la structure d’un arbre phylogénétique ?

## Est que je sais expliquer le caractère buissonnant de l’évolution de l’espèce humaine à travers des documents ?

## Est que je sais decrire les mécanismes importants impliqués dans la théorie de l’évolution ? (Variabilité génétique, sélection naturelle)

## Est que je sais identifier les critères anatomiques d’appartenance à la lignée humaine ?

## Est que je sais situer et dater l’origine de la lignée humaine ?

## Est que je sais interpréter la transmission d’un caractère dans un arbre généalogique humain ?

## Est que je sais établir la relation entre phénotype et séquences d’ADN dans le cas d’une maladie génétique ?

## Est que je sais mettre en évidence les liens de parentés entre des etres vivants à partir de données anatomiques, embryonnaires, moléculaires, paléontologiques ?


UAA6

Savoirs

## Est que je sais définir les mots suivants : écosystèmes, réseaux trophiques
Un **écosystème** est un mot qui regroupe l'environement et les êtres vivants qui sont dessus. 

Un **biotope** 

La **biosphère**

Un **réseau trophique** est un ensemble de chaines alimentaires. 

## Est que je sais citer 5 causes principales de la diminution de la biodiversité d’un écosystème ?

## Est que je sais expliquer ce qu’est l’empreinte écologique ?

## Est que je sais citer des services rendus par les écosystèmes ?

Est. E que je sais définir les mots suivants : espèce menacée et espèce invasive ?

Savoir faire et compétences

## Est que je sais construire un argumentaire scientifique sur le développement durable ?

## Est que je sais identifier l’impact significatif des activités humaines sur un écosystème ?

## Est que je sais identifier les causes à l’origine d’une diminution de la biodiversité à l’aide de documents ?

## Est que je sais développer une argumentation scientifique pour critiquer l’action de l’Homme sur un écosystème et proposer des solutions préventives et curatives ?

## Est que je sais decrire les caractéristiques qui permettent de classer une espèce comme étant menacée ?

## Est que je sais expliquer comment certaines activités humaines favorisent le développement , le maintient ou la restauration de la biodiversité ?

## Est que je sais decrire les caractéristiques qui permettent de classer une espece comme étant invasive ?

## Est que je sais expliquer la notion d’empreinte écologique ?

## Est que je sais montrer la nécessité de préserver les écosystèmes par rapport à leurs services rendus ?

## Est que je sais expliquer comment certaines activités humaines peuvent modifier le fonctionnement d’un écosystème ?

## Est que je sais calculer mon empreinte écologique ?
