# Spectroscopie
## Définitions
> La **spectroscopie** est l'analyse quantitative du lien entre la lumière et la matière.

## Formules
| Symbole | Signification | Unité |
| --- | --- | --- |
| $A$ | Absorbance | |
| $\Epislon$ | Coefficient d'extinction molaire | |
| $l$ | Longueur traversée |  |
| $C$ | Concentration | |
