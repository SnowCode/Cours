Chimie recapitulatif

Uaa 5 


Savoirs

## Est-ce que je sais expliquer le modèle de Lewis ?
Permet de visualiser les liaisons chimiques.

## Est-ce que je sais définir un électron de Valence ?
C'est un électron qui se trouve sur la dernière couche électronique de l'atome.

## Est-ce que je sais définir liaison ionique et ses caractéristiques ?
Une liaison ionique est une liaison chimique formée par deux atomes avec une forte différence d'électronégativité (>1.7)

## Est-ce que je sais définir liaison covalente pure et ses caractéristiques ?
Une liaison covalente pure ou non-polaire est une liaison chimique formée par deux atomes avec une très faible électronégativité (<0.49).

## Est-ce que je sais définir liaison covalente polarisée et ses caractéristiques ?
Une liaison covalente polarisée est une liaison chimique formée par deux atomes avec une électronégativité entre 0.49 et 1.7.

Savoir faire et compétences 

## Est-ce que je sais représente la structure de Lewis d’un atome ?
Oui INSERER IMAGE ICI

## Est-ce que je sais représente les liaisons au sein d’une molécule en utilisant le modèle de Lewis et le tableau périodique ? 
OUI INSERER IMAGE ICI

## Est-ce que je sais représenter une molécule en 3D ? 
IMAGE ICI

---- -> Même plan que la feuille 
NNNN -> En avant
|||| -> En arrière

## Est-ce que je sais reprensetne la configuration spatiale d’une molécule (VSEPR) et son comportement dans l’eau ?
[Vidéo pour comprendre la réprésentation spaciale](https://www.youtube.com/watch?v=2NZljP37cA8)

[Article Wikipedia](https://fr.wikipedia.org/wiki/Th%C3%A9orie_VSEPR)

## Est-ce que je sais decrire la structure électronique externe d’un atome par rapport à sa position dans le tableau ?
Oui gràace au colonnes.

## Est-ce que je sais identifier le type de liaison à partir de l’electronegativite des atomes ?
Insert Ascii here

## Est-ce que je sais écrire l’équation de dissociation des sels ?


UAA6

Savoirs 

    1) ## Est-ce que je sais définir : concentration molaire et constante d’equilibre

    2) ## Est-ce que je sais donner la formule et l’utiliser : concentration molaire et constante d’équilibre 

    3) ## Est-ce que je sais expliquer la loi de Le chatelier ?

    4) ## Est-ce que je sais expliquer la loi de Guldberg et Waage ?

    5) ## Est-ce que je sais expliquer à quoi correspond une réaction complète ? 

    6) ## Est-ce que je sais expliquer à quoi correspond une réaction limitée à un état d’équilibre ?

Savoir faire et compétences 

## Est-ce que je sais aller chercher les informations dans une table de thermodynamie ?

## Est-ce que je sais prévoir le sens d’évolution d’une réaction réversible ?

## Est-ce que je sais résoudre une équation mathématique du premier et second degré afin de résoudre un exercice de chimie ?

## Est-ce que je sais utiliser le principe de Le chatelier à partir d’un exemple ?

## Est-ce que je sais utiliser la formule d Léa constante d’équilibre afin de la calculer ou de retrouver les concentrations ?

## Est-ce que je sais calculer une concentration molaire ?

## Est-ce que je sais prévoir le sens spontané d’évolution d’une réaction chimique initialement à l’équilibre ?






UAA7 

Savoirs 

    1) ## Est-ce que je sais définir les mots suivants : composé organique, alcane, alcène, combustible, comburant, combustion ?

    2) ## Est-ce que je sais définir le pouvoir calorifique ?

    3) ## Est-ce que je sais définir les mots suivants : monomère, polymères, 

    4) ## Est-ce que je sais reconnaître les pictogrammes des polymères  ?

Savoir faire et compétences 

## Est-ce que je sais utiliser une table calorifique pour en y retirer les informations ?

## Est-ce que je sais évaluer l’importance des substances organiques présentes dans le quotidien et dans l’environnement ?

## Est-ce que je sais faire la différence entre un composé organique et un composé inorganique ?

## Est-ce que je sais montrer l’impact positif des polymères synthétiques sur notre société ?

## Est-ce que je sais decrire le phénomène de combustion ? Et écrire une équation de combustion ?

## Est-ce que je sais expliquer le processus de recyclage des matières plastiques ?

## Est-ce que je sais retracer les étapes du processus industriel qui permet de fabriquer des carburants ?

## Est-ce que je sais decrire le principe d’une réaction de polymérisation ?

## Est-ce que je sais faire le lien entre les macromolécules naturelles et la réaction de polymérisation ?

## Est-ce que je sais decrire les polymeres synthétiques à partir de leur pictogrammes 

## Est-ce que je sais identifier les combustibles qui rejettent le moins de CO2, les plus économiques à partir d’une table calorifique ?

## Est-ce que je sais écrire les différentes formules d’un alcane ?



UAA8 

Savoirs 

    1) ## Est-ce que je sais expliquer ce qu’est un acide selon Bronsted ?

    2) ## Est-ce que je sais expliquer ce qu’est une base selon Bronsted ?

    3) ## Est-ce que je sais écrire une réaction de neutralisation selon Arrhenius ?

    4) ## Est-ce que je sais écrire l’équation d’autoprotolyse de l’eau ?

    5) ## Est-ce que je sais définir ce qu’est un couple acide/base,

    6) ## Est-ce que je sais expliquer ce qu’est le pH ?

    7) ## Est-ce que je sais définir la neutralisation ?

    8) ## Est-ce que je sais définir ce qu’est un oxydant ?

    9) ## Est-ce que je sais définir ce qu’est un réducteur ?

    10) ## Est-ce que je sais définir ce qu’est la réaction d’oxydation ?

    11) ## Est-ce que je sais définir ce qu’est la réaction de réduction ?

    12) ## Est-ce que je sais définir ce qu’est un couple oxydant/réducteur ?

    13) ## Est-ce que je sais à quoi correspond la table des potentiels redox et est ce que je sais l’utiliser ?

    14) ## Est-ce que je sais définir ce qu’est une pile ?

    15) ## Est-ce que je sais deifnir les mots suivants : solubilité, espèce soluble, espèce insoluble, espèce peu solubles, précipitation ?

    16) Est. E que je sais utiliser un tableau de solubilité ?

Savoir faire et compétences 

## Est-ce que je sais utiliser les logarithmes base 10 ? Et sur ma calculette ?

## Est-ce que je sais decrire une réaction de précipitation et l’écrire ?

## Est-ce que je sais écrire et décrire une réaction acide base en mettant en évidence les transferts de protons ?

## Est-ce que je sais déterminer la charge d’un ion à attirer du tableau périodique ?

## Est-ce que je sais écrire et décrire une réaction redox en mettant en évidence le transfert d’électrons ?

## Est-ce que je sais écrire et décrire une réaction de réduction ?

## Est-ce que je sais écrire et décrire une réaction d’oxydation ?

## Est-ce que je sais expliquer le fonctionnement de la pile emettant en évidence les équations à chaque borne ?

## Est-ce que je sais faire le lien entre la valeur du pH d’un environnement et les comportements ou les propriétés de ce milieu ?

## Est-ce que je sais utiliser la table des potentiels dans un cas concret ?

## Est-ce je sais decrire le phénomène de corrosion comme étant une réaction d’oxydoreduction ?

## Est-ce je sais faire le lien entre une situation concrète et un phénomène de précipitation ?

## Est-ce que je sais decrire et illustrer l’échelle des pH ?

## Est-ce que je sais prévoir une réaction de précipitation en utilisant la table des precipitation ?

## Est-ce que je sais déterminer les espèces chimiques présentes à partir des espèces introduites ?

## Est-ce que je sais prévoir l’évolution d’une réaction d’oxydo réduction en utilisant la table des potentiels ?

## Est-ce que je sais faire le lien entre une situation de la vie courante et une réaction de neutralisation ?





