Savoirs 

## Est-ce que je sais repérer la pente d’une droite ?

$$
\Delta d = \frac{\Delta Y}{\Delta X} = \frac{y_2 - y_1}{x_2 - x_1}
$$

## Est-ce que je sais ce qu’est un vecteur ?



3) ## Est-ce que e sais définir la vitesse ?

4) ## Est-ce que je sais définir ce qu’est une force ? 

5) ## Est-ce que sais définir et indiquer sur un schéma les forces de frottements ?

6) ## Est-ce que je sais schématiser et décorer le principe des actions réciproques ?

7) ## Est-ce que je sais définir et calculer l’énergie cinétique d’un objet ?

8) ## Est-ce que je sais représenter la tangente à une courbe ?

9) ## Est-ce que je sais définir les mots suivants : referentiel, vitesse moyenne, vitesse instantanée, accélération moyenne, accélération instantanée ?

10) ## Est-ce que je sais donner les formules suivantes : vitesse moyenne, vitesse instantanée, accélération moyenne, accélération instantanée ?

11) ## Est-ce que je sais définir le  mouvement rectiligne uniforme en donnant ses caractéristiques ?

12) ## Est-ce que je sais définir le mouvement rectiligne uniforme accéléré et ses caractéristiques ?

13) ## Est-ce que je sais calculer des distances à partir de la vitesse ?

14) ## Est-ce que je sais expliquer et citer les trois lois de Newton ?

## Est-ce que je sais caractériser le vecteur vitesse dans le cas du MRU circulaire ?

## Est-ce que je sais caractériser le vecteur vitesse angulaire dans le cas du MRU circulaire ?

## Est-ce que je sais définir les mots suivants : accélération centripète et force centripète ?

Savoir faire et compétences 

## Est-ce ce que je sais sur un graphique position temps repérer la vitesse ?

## Est-ce que je sais convertir et interpréter des graphiques de mouvements (MRU et MRUA) ?

## Est-ce que je sais sur un graphique vitesse temps identifier l’accélération ?

## Est-ce que je sais utiliser les lois de la physique dans le cadre de la sécurité routière ?

## Est-ce que je sais calculer une vitesse moyenne ?

## Est-ce que je sais justifier une affirmation de sécurité routière par les lois de la physique ?

## Est-ce que je sais calculer une accélération moyenne ?

## Est-ce que je sais faire le lien entre les lois de Newton et les éléments de la sécurité routière ?

## Est-ce que je sais représenter les forces agissant sur un objet en lien avec son mouvements ?

## Est-ce que je sais caractériser un mouvement en termes de forces et de vitesse ?

## Est-ce que je sais citer les unités du système international en lien avec les forces ?

## Est-ce que je sais faire le lien entre la relativite du mouvement et la trajectoire dans deux référentiels différents ?

## Est-ce que je sais decrire un mouvement circulaire uniforme en précisant les caractéristiques suivantes : vitesse, accélération et force centripète ?

## Est-ce que je sais construire un graphique de position et d’accélération à partir d’un graphique et de justifier la forme des courbes obtenues ?

## Est-ce que je sais decrire l’évolution de la vitesse de chute d’un objet ?





UAA6 

Savoirs

## Est-ce que je sais définir les mots suivants : période, fréquence, longueur d’onde, propagation, élongation ?
La **période** est un motif répétitif, elle se note $T$ et son unité est la seconde.

La **fréquence** correspond à $\frac{1}{T}$, elle se note $f$ ou $v$ et son unité est le Hertz.

La **longueur d'onde** est la distance séparant deux maxima consécutifs de l'amplitude, son symbole est $\lambda$, son unité est le $m$ et sa formule est $\lambda = cT = \frac{c}{f}$

La **propagation des ondes** est un phénomène physique dont découlent l'évolution et la progression d'une onde au sein d’un milieu

Il y a plusieurs types de propagation d'onde: voici les deux types :

![transversal et longitudinal](https://media.kartable.fr/uploads/finalImages/final_55350e75532e06.60789994.png)

## Est-ce que je sais définir vitesse de propagation ?
La **vitesse de propagation** ou **célérité** correspond à la vitesse de l'onde et s'exprimme en m/s (mètre par seconde).

## Est-ce que je sais définir les mots suivants : milieu de propagation, concordance de phase et opposition de phase ?
Le **milieu de propagation** est le 

## Est-ce que je sais définir les mots suivants :réflexion, réfraction, diffraction, résonance, interférence 
La **diffraction** correspond à la trajectoire d'une onde déviée par un obstacle. Son équation est $\Teta = \frac{\lambda}{a}$, $a$ est l'ouverture de l'obstacle (en mètre). Une autre formule est: $\Teta=\frac{L}{2D}$

![diffraction](./diffraction.png)

La **réfraction** 

La **réflexion** 

## Est-ce que je sais définir et expliquer l’effet Doppler ?

## Est-ce que je sais définir spectre électromagnétique ?

## Est-ce que je sais définir l’induction magnétique ?

## Est-ce que je sais caractériser les ondes sonores ( intensité, niveau, audibilité) ?


Savoir faire et compétences 

## Est-ce que je sais calculer une fréquence à partir d’une période et inversement ?

## Est-ce que je sais decrire un phénomène impliquant la transmission des ondes ?

## Est-ce que je sais expliquer un phénomène impliquant la transmission des ondes ?

## Est-ce que je sais appliquer la transformation suivante :

## Est-ce que je sais expliquer comment utiliser les propriétés des ondes à partir d’un document ?

## Est-ce que je sais aller chercher des informations sur les effets d’un type d’onde et les critiquer ?

## Est-ce que je sais utiliser les unités du système international ?

## Est-ce que je sais citer des exemples de phénomène périodique ?

## Est-ce que je sais citer des exemples de phénomènes ondulatoire ?

## Est-ce que je sais faire le lien entre l’énergie transportée par une onde et son amplitude ?
La formule de l'énergie est:

$$
E = \frac{hc}{\lambda} = hf
$$

| Symbole   | Signification         |   
| --------- | -------------         | 
| $E$       | L'énergie             |
| $h$       | La constante de plank |
| $\lambda$ | Longueur d'onde       |
| $c$       | Célérité              |

Pour calculer l'énergie transportée : 

$$
\Delta E = E_p - E_n
$$

## Est-ce que dans un document je sais identifier les propriétés des ondes ?

## Est-ce que je sais estimer la distance d’impact de la foudre en utilisant la vitesse du son et de la lumière ?

## Est-ce que je sais decrire comment on produit et capte une onde électromagnétique ?


UAA7



Savoirs

    1) ## Est-ce que je sais donner les caractéristiques de chaque rayonnement (origine nucléaire, activité, type) ?

    2) Est e que je sais définir la demi vie d’un échantillon et la calculer ?

    3) ## Est-ce que je connais l’unité du système international ?

    4) Est ce que je sais deifnir le terme fission nucléaire ?

    5) ## Est-ce que je sais decrire les produits de fission ?

    6) ## Est-ce que je sais définir les mots suivants : courant induit, génératrice, transformateur ?

    7) ## Est-ce que je sais decrire la distribution de l’énergie électrique ?

    8) ## Est-ce que je sais énoncé le premier principe de la thermodynamique ?

    9) ## Est-ce que je sais decrire les énergies renouvelables et les non renouvelables ?

    10) ## Est-ce que je sais définir ce qu’est une machine thermique ?

    11) ## Est-ce que je sais décidé ce qu’est le rendement d’une machine ?

Savoir faire et compétences 

## Est-ce que je sais appliquer le premier principe de la thermodynamique ?

## Est-ce que sais expliquer comment on produit de l’énergie électrique ?

## Est-ce que je sais expliquer comment on transporte de l’énergie électrique ?

## Est-ce que je sais calculer le rendement  d’une machine ?

## Est-ce que je sais présenter les avantages et les inconvénients des énergies renouvelables et non renouvelables ?

## Est-ce que je sais réaliser un schéma électrique ?

## Est-ce que je sais utiliser les unités  du système international ?

## Est-ce que je sais citer les  conditions d’apparition d’un courant induit produit par une génératrice ?

## Est-ce que je sais à partir d’un schéma d’une machine thermique expliquer les transferts d’énergies ?

## Est-ce que je sais réaliser un schéma en présentant les énergies entrantes et sortantes d’une machine ?

## Est-ce que je sais expliquer le fonctionnement  d’un réacteur nucléaire et décrire la production d’énergie électrique associée ?

## Est-ce que je sais decrire le fonctionnement d’une machine thermique et expliquer pourquoi le rendement est toujours inférieur à 100% ?

## Est-ce que sais déterminer la demi vie d’un échantillon à partir d’un graphique présentant la décroissance radioactive en fonction du temps ?

## Est-ce que je sais calculer le rendement théorique et le rendement effectif d’une machine thermique ?



UAA8

Savoirs 

    1) ## Est-ce que je sais définir les mots suivants : geocentrisme, heliocentrisme, vitesse de la lumière, force de gravitation universelle ?

    2) ## Est-ce que je sais à quoi correspond les astres suivants et leurs caractéristiques : Terre, Soleil, Lune, système solaire ?

    3) ## Est-ce que je sais définir les mots suivants : étoiles, galaxies, 

    4) ## Est-ce que je sais expliquer la notion de fusion nucléaire ?

    5) ## Est-ce que je sais expliquer les hypothèses du Big Bang ?

    6) ## Est-ce que je sais expliquer la naissance et l’évolution d’une étoile ?

    7) ## Est-ce que je sais donner les caractéristiques de la Terre( températures, dimension, structure, atmosphère) ?

    8) ## Est-ce que je sais expliquer ce qu’est l’effet de serre ?

    9) ## Est-ce que je sais expliquer ce qu’est le bilan radiatif moyen ?

Savoir faire et compétences 

## Est-ce que je sais appliquer la loi de la gravitation universelle ?

## Est-ce que je sais decrire la place de la Terre dans l’Univers ?

## Est-ce que je sais utiliser les unités du système international ?

## Est-ce que je sais expliquer les caractéristiques qui rendent la terre habitable ?

## Est-ce que je sais decrire les grandes étapes de l’évolution es modèles relatifs aux mouvements des astres ?

## Est-ce que je sais aller chercher des informations qui me permettent d’estimer l’influence de l’evolution de la composition de l’atmosphère sur l’effet de serre ?

## Est-ce que je sais decrire la structure du système solaire, des planètes et des orbites ?

## Est-ce que je sais aller chercher les informations qui permettent de décrire les caractéristiques physiques qui ont permis le développement de la vie sur terre ?

## Est-ce que je agis expliquer comment on mesure la distance astronomique ?

## Est-ce que je sais decrire l’histoire de l’Univers et l’évolution des étoiles’ ?

## Est-ce que je sais calculer la variation e l’accélération de la pesanteur en fonction de l’altitude ?

## Est-ce que je sais estimer la valeur de la vitesse de la lumière à travers différentes pratiques expérimentales ?
