## Définition
> Un **vecteur** est un objet mathématique caractérisé par une **direction** (haut, bas),  un **sens** (gauche, droite) et une **longueur** (aussi appellé **norme**). 

## Comparer des vecteurs
Des vecteurs égaux sont des vecteurs qui ont la même direction, le même sens et la même longueur.

$$
\vec{u_1} = \vec{u_2}
$$

Des vecteurs sont dit *opposés* quand ils ont la même direction, la même longueur mais un sens contraire.

$$
\vec{u_1} = -\vec{u_2}
$$

## Représenter un vecteur
Si on a dans un graphique un vecteur avec deux points, $A(-2;3)$ et $B(4;-1)$

Le vecteur $\vec{AB}$ sera: 

$$
\vec{AB} = (x_b - x_a ; y_b - y_a)
$$

$$
\vec{AB} = (4-(-2) ; -1-3)
$$

$$
\vec{AB} = (6;-4)
$$


## Faire des opérations avec les vecteurs
### Additiionner et soustraires deux vecteurs
Nous avons ici deux vecteurs : 

$$
\vec{AB} = (6;-4)
$$

$$
\vec{CD} = (5;2)
$$

Donc pour faire $\vec{AB} + \vec{CD}$ on va additionner les composantes:

$$
\vec{AB} + \vec{CD} = (6+5;-4+2)
$$

$$
\vec{AB} + \vec{CD} = (11;-2)
$$


### Multiplier et diviser un vecteur par un réel
Pour faire $5 * \vec{AB}(6;5)$ il suffit de multiplier les composantes par le nombre.

$$
5\vec{AB} = (6 * 5 ; 5 * 5)
$$

$$
5\vec{AB} = (30;25)
$$

### Trouver le milieu d'un segment (ou d'un vecteur)

Voici la formule pour trouver le milieu d'un segment. 

$$
M(\frac{x_A+b_B}{2},\frac{y_A+y_B}{2})
$$

### Savoir si deux vecteurs sont // (colinéaires)
Deux vecteurs ($u$ et $v$) sont colinéaires dans le cas où l'expression suivante est vraie:

$$
x_v * y_u - x_u * y_v = 0
$$

### Savoir si deux vecteurs sont perpendiculaires (orthogonalité)
DDeux vecyeurs ($u$ et $v$) sont orthogonaux dans le cas où l'expression suivante est vraie:

$$
x_u * x_v + y_u * y_v = 0
$$

### Connaître la longueur d'un vecteur
La longueur d'un vecteur se note $||u||$ et pour la connaître on fait:

$$
||\vec{u}|| = \sqrt{x_u^2 + y_u^2}
$$

Oui bien

$$
||\vec{AB}|| = \sqrt{(x_B - x_A)^2 + (y_B-y_A)^2}
$$
