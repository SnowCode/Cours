> Une **suite** est une liste ordonnée, finie ou infinie de nombres réels. Les différents nombres de cette liste sont appellé les *termes* de la suite.

## Suite algébrique
Dans une suite géométrique les nombres sont à chaquefois, le nombre précédent ajouté ou soustrait d'un autre nombre appellé *raison*.

La formule simplifie de la suite algébrique est donc:

$$
u_n = u_{n-1} + r
$$

Dans cette équation plusieurs lettres sont utilisées:

| Lettre utilisée | Signification |
| --- | --- |
| $u$ | Terme de la suite |
| $n$ | Indice de la suite | 
| $u_n$ | Terme de la suite de rang $n$ |
| $u_{n-1}$ | Terme de la suite qui précède $n$ |
| $r$ | Le coefficient, appellé *raison*, dans une suite **algébrique** |
| $q$ | Le coefficient, appellé *raison*, dans une suite **géométrique** |
| $u_1$ | Le premier terme de la suite |

Mais il existe aussi une autre formule appellée *formule explicite*, la méthode précédente étant appellé *formule réccurente*. 

$$
u_n = u_1 + r(n-1)
$$

A partir de maintenant on sait donc trouver un nombre de la suite en partant soit de la raison $r$ et du nombre précédent $u_{n-1}$. Soit en utilisant le premier nombre de la suite $u_1$ et la raison $r$.

### Trouver un bombre à l'aide des deux autres qui l'encadrent
Pour faire cela on utilise la formule

$$
u_n = \frac{u_{n-1} + u_{n+1}}{2}
$$

### Calculer la somme de $n$ termes de la suite

$$
S = \frac{n(u_1+u_n}{2}
$$

## Suite géométrique
Dans une suite géométrique les nombres sont à chaquefois, le nombre précédent est multiplié ou divisé d'un autre nombre appellé *raison*.

Sa formule par *réccurence* est

$$
u_n = u_{n-1} * q
$$

Et sa formule *explicite* est

$$
u_n = u_1 * q^{n-1}
$$

### Trouver un bombre à l'aide des deux autres qui l'encadrent

$$
u_n = \sqrt{u_{n-1} * u_{n+1}}
$$

### Calculer la somme de $n$ termes de la suite

$$
S = u_1 * \frac{1-q^n}{1-q}
$$
