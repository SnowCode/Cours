## Formules
La formule du cercle trigonométrique

$$
\sin^2{\alpha} + \cos^2{\alpha} = 1
$$

La formule de la tangeante

$$
\tan{\alpha} = \frac{sin{\alpha}}{\cos{\alpha}}
$$

### La formule des sinus

$$
\frac{a}{\sin{\alpha}} = \frac{b}{\sin{\beta}} = \frac{c}{\sin{\gamma}}
$$

### La formule des cosinus

$$
a^2 = b^2 +c^2 - 2bc\cos{\alpha}
$$

$$
b^2 = a^2 + c^2 - 2ac\cos{\beta}
$$

$$
c^2 = a^2 + b^2 - 2ab\cos{\gamma}
$$

### Somme des angles d'un triangle

$$
\alpha = 180° - \beta - \gamma
$$

### L'aire d'un triangle

$$
aire = \frac{bc\sin{\alpha}}{2} = \frac{ac\sin{\beta}}{2} = \frac{ab\sin{\gamma}}{2} 
$$

### Convertir en angle / minutes / secondes

$$
1° = 60' = 3600''
$$

$$
1' = 1/60° = 60''
$$

$$
1'' = 1/3600° = 1/60'
$$

### Convertir des angles en radian

$$
180° = \pi
$$

$$
1° = \frac{\pi}{180} = 0.0175
$$

$$
\frac{180°}{\pi} = 57.3° = 1
$$

## Savoir quand utiliser les formules
| Si on connaît | On utilise |
| --- | --- |
| Deux angles et un côté | La somme des angles et la formule des sinus |
| Deux côtés et l'angle entre les deux | La formule des cosinus |
| Trois côtés | La formule des cosinus |

## Comment les utiliser
Cette section n'est pas encore faite

## Les fonctions trigonométriques
Voici une table pour comprendre les lettres dans les formules

| Lettre | Signification |
| --- | --- |
| $A$ | L'amplitude (grandeur mesurée) |
| $\omega$ | Vitesse angulaire, pulsation par seconde, exprimée en radian |
| $\phi$ | La phase d'origine |
| $T$ | Durée d'un cycle. Equivaut à $\frac{2\pi}{\omega}$ |
| $f$ | Fréquence, exprimée en Hertz, au nombre de périodes par seconde, elle vaut $\frac{1}{T}$ |

Le tout est lié par la relation

$$
\omega = 2 \pi f = \frac{2\pi}{T}
$$
