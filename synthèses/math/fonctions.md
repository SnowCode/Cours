## Trouver l'ordonnée à l'origine et les conditions d'existence

| Calcul                      | Explication                                 |
| --------------------------- | ------------------------------------------- |
| $f(x)=\frac{x^2+2x+1}{x-1}$ | L'énoncé                                    |
| $x-1 \neq 0$                | Le dénominateur ne peut pas être égal à $0$ |
| $x -1+1\neq 0+1$            | Résoudre l'équation                         |
| $x\neq1$                    | Conditions d'existence finale.              |

*Donc l'ordonnée à l'origine est $1$ et la condition pour l'existence de la fonction est que $x$ ne peux pas valoir $0$*

## Trouver la parité d'une fonction

| Calcul                                | Explication                                                                                                                                     |
| ------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------- |
| $f(x)=\frac{x^2+2x+1}{x-1}$           | L'énoncé                                                                                                                                        |
| $f(-3)=\frac{(-3)^2+2(-3)+1}{-3-1}$   | Remplacer $x$ par un nombre négatif qui respecte les conditions d'existence                                                                     |
| $f(-3)=-1$                            | Effectuer le calcul                                                                                                                             |
| $f(3)=\frac{3^2+2*3+1}{3-1}$          | Remplacer $x$ par le  même nombre que le précédent mais positif.                                                                                |
| $f(3)=8$                              | Effectuer le calcul                                                                                                                             |
| La fonction n'est ni paire ni impaire | Si $f(-x)=f(x)$ alors la fonction est paire<br/>Si $f(-x)=-f(x)$ alors la fonction est impaire<br/>Sinon, la fonction n'est ni paire ni impaire |

*La fonction n'est ni paire ni impaire*

## Trouver les racines

| Calcul               | Explication                                                                                        |
| -------------------- | -------------------------------------------------------------------------------------------------- |
| $f(x)=\frac{x+1}{x}$ | L'énoncé                                                                                           |
| $x+1=0$              | Prendre uniquement le numérateur de la fonction pour faire une équation sous forme: $numerateur=0$ |
| $x+1-1=-1$           | Résoudre l'équation                                                                                |
| $x=-1$               | La racine de la fonction est $-1$                                                                  |

*La seule racine de la fonction est en $x=-1$*

## Faire un tableau de signe

| Calcul                                                                                    | Explication                                                                |
| ----------------------------------------------------------------------------------------- | -------------------------------------------------------------------------- |
| *On sait que la racine de la fonction est $x=0$ et la condition d'existence est $x\neq0$* | L'énoncé                                                                   |
| `\|   \| -1 \|   \| 0 \|   \|`                                                            | Faire le haut du tableau avec les racines et les conditions d'existences   |
| `\| - \| 0  \| - \| E \| + \|`                                                            | Ajouter les valeurs qui correspondent. Ainsi que les signes avant et après |

|     | -1  |     | 0          |     |
|:---:|:---:|:---:|:----------:|:---:|
| -   | 0   | -   | $\nexists$ | +   |

## Calculer les asymptotes
Pour calculer les asymptotes il est très important de les faires dans l'ordre. D'abord essayer de trouve une asymptote verticale, puis essayer de trouver une asymptote horizontale.
Si il y a une asymptote horizontale ça veux dire qu'il n'y a pas d'asymptote oblique donc pas besoin de faire le calcul.

### Asymptote verticale

| Calcul                                 | Explication                                                                        |
| -------------------------------------- | ---------------------------------------------------------------------------------- |
| $f(x)=\frac{x^2-3x+4}{x-1}$            | L'énoncé                                                                           |
| $x\neq1$                               | Calculer les conditions d'existence                                                |
| $\lim_{x\to1^-} \frac{1^2-3*1+4}{1-1}$ | Mettre la limite vers la valeur trouvé en conditions et remplacer dans la fonction |
| $\lim_{x\to1} \frac{2}{0^-}$           | Simplifier la fonction                                                             |
| $-\infty$                              | Transformer le $0^-$ en $-\infty$                                                  |

### Asympotes horizontales

| Calcul                                           | Explication                                                                                                                                   |
| ------------------------------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------- |
| $f(x)= \frac{2x+3}{x^2+4}$                       | L'énoncé                                                                                                                                      |
| $\lim_{x\to\infty} \frac{2\infty+3}{\infty^2+4}$ | Remplacer (mentalement) $x$ par $\infty$                                                                                                      |
| $\lim_{x\to\infty} \frac{2x}{x^2}$               | C'est le cas de $\frac{\infty}{\infty}$. Donc il faut prendre la plus grande puissance ainsi que son *coefficient* (nombre qui est multiplié) |
| $\lim_{x\to\infty} \frac{2}{x}$                  | Simplifier la fraction                                                                                                                        |
| $AH=0$                                           | La forme $\frac{1}{x}$ est égale à $0$. La résponse est donc $0$                                                                              |

### Asymptotes oblique

| Calcul                                            | Explication                                                                                                                          |
| ------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------ |
| $f(x)=\frac{x^3-x^2+5x-1}{x^2+1}$                 | L'énoncé                                                                                                                             |
| $\lim_{x\to\infty} \frac{x^3-x^2+5x-1}{x(x^2+1)}$ | Appliquer la formule $\lim_{x\to\infty}\frac{f(x)}{x}$ pour trouver $m$ (la pente)                                                   |
| $\lim_{x\to\infty}\frac{x^3}{x^3}$                | C'est le cas $\frac{\infty}{\infty}$. Donc il faut mettre en évidence en gardant uniquement le plus grand exposant (voire plus haut) |
| $m=1$                                             | Simplifier pour obtenir la pente                                                                                                     |

| Calcul                                                                  | Explication                                                                                        |
| ----------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------- |
| $\lim_{x\to\infty}\frac{x^3-x^2+5x-1}{x^2-1}-x$                         | Appliquer la forumle $\lim_{x\to\infty}\frac{f(x)}+x$ pour trouver $p$ (l'ordonnée à l'origine     |
| $\lim_{x\to\infty}\frac{x^3-x^2+5x-1}{x^2-1}-\frac{x(x^2-1)}{1(x^2-1)}$ | Mettre au même dénominateur en multipliant le dénominateur du premier terme avec tout le deuxième. |
| $\lim_{x\to\infty}\frac{(x^3-x^2+5x-1)-(x(x^2-1))}{x^2-1}$              | Mettre sous la même barre de fraction                                                              |
| $\lim_{x\to\infty}\frac{x^3-x^2+5x-1-x^3-x}{x^2-1}$                     | Développer                                                                                         |
| $\lim_{x\to\infty}\frac{-x^2+4x-1}{x^2-1}$                              | Simplifier                                                                                         |
| $\lim_{x\to\infty}\frac{-x^2}{x^2}$                                     | Mettre en évidence en gardant seulement la plus grande puissance de $x$, voire plus haut           |
| $p=-1$                                                                  | Simplifier la fraction                                                                             |

| Calcul                | Explication                                                                     |
| --------------------- | ------------------------------------------------------------------------------- |
| $AO \equiv y=1x+(-1)$ | Appliquer la formule $AO \equiv y=mx+p$ à partir des informations vue çi dessus |
| $AO \equiv y=x-1$     | Simplifier, résultat final                                                      |


## Dérivés
### Première dérivée (Savoir si ça monte, dessend ou reste stable)

| Calcul | Explication | 
| --- | --- |
| $f'(x)=(\frac{x^2+2x+1}{x})'$ | L'énoncé |
| $f'(x)=\frac{(x^2+2x+1)'(x)-(x^2+2x+1)(x)'}{x^2}$ | Appliquer la formule $(\frac{f}{g})'=\frac{f'g-fg'}{g^2}$ |
| $f'(x)=\frac{(2x+2+0)(x)-(x^2+2x+1)(1)}{x^2}$ | Appliquer la formule $x^n=nx^{n-1}$ ou autres formules (voir plus bas). Faire les dérivés |
| $f'(x)=\frac{2x^2+2x-x^2-2x-1}{x^2}$ | Développer et distribuer |
| $f'(x)=\frac{x^2-1}{x^2}$ | Simplifier |
| Faire un tableau de signe | Voir plus haut. | 

| $x$     |       | $-1$    |          | $0$        |          | $1$     |       |
| ------- | ----- | ------- | -------- | ---------- | -------- | ------- | ----- |
| $f'(x)$ | +     | 0       | -        | $\nexists$ | -        | 0       | +     |
| $f(x)$  | Monte | Maximum | Dessends | $\nexists$ | Dessends | Minimum | Monte |

### Deuxième dérivée (Savoir si c'est concave ou convexe)

| Calcul | Explication |
| --- | --- |
| $f''(x) = (\frac{x^2-1}{x^2})'$ | La dérivée précédente (dérivée première) |
| $f''(x) = \frac{(x^2-1)'(x^2)-(x^2-1)(x^2)'}{x^4}$ | Appliquer la formule $(\frac{f}{g})'=\frac{f'g-fg'}{g^2}$ |
| $f''(x) = \frac{(2x-0)(x^2)-(x^2-1)(2x)}{x^4}$ | Appliquer la formule $x^n=nx^{n-1}$ ou autres formules (voir plus bas). Faire les dérivés |
| $f''(x) = \frac{2x^3-(2x^3-2x)}{x^4}$ | Distribuer |
| $f''(x) = \frac{2}{x^3}$ | Simplifier |
| Faire un tableau de signe | Voir plus haut. | 

| $x$ |     | $0$ |     |
| --- | --- | --- | --- | 
| $f'(x)$ | - | $\nexists$ | + |
| $f(x)$ | Concave | $\nexists$ | Convexe |

### Définition d'une dérviée 
TODO

### Faire une t
TODO
