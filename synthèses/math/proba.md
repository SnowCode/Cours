Propriétés

$$
0 < P(A) < 1
$$

$$
P(A) = \đrac{nombre de cas favorables}{nombre de cas possible}
$$

$$
P(1) + P(2) = 1
$$

$$
P(A) = 1 - P(A)
$$

## La loi binomiale
C'est quand il n'y a que deux possibilités: échec ou réussite. 

$$
P(X=k)=\frac{n!}{k!(n-k)!}p^k(1-p)^{n-k}
$$

| Mathématiques | Français |
| -------- | ----------------------- |
| $P(X=k)$ | Probabilité de réussite |
| $k$ | Nombre de succès |
| $n$ | Nombre de répétitions |
| $p$ | Probabilité (0,xxx) |
