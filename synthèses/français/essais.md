# Le complotisme au XXIe siècle
## Les définitions
> **épistémiologique**, signifie, relatif à la conaissance, le savoir, les sciences.

> Un **consensus** c'est quand tout le monde peut d'accorder sur un fait.

> **Définition heuristique**, qui sert à la découverte, de base. Il faut bien commencer quelque pars.

> **Prototypique**, de base, idéale.

## Vers une impossible définition
### Pourquoi c'est si difficile
* Le terme *complotisme* est très difficile à définir.
    * Car c'est un terrain glissant par rapport à la question **épistémiologique** (fondamentale) du status de la réalité.
        * Cela signifie que pour un complotiste, le fait que le complot est avéré ou non n'a pas d'importance, le simple fait que le complot puisse être vrai est suffisant.

* Il y a un un seul **consensus** sur le complotisme qui est que le mot *complotiste* est péjoratif.

* Le complotisme est porteur d'enjeu aussi
    * Séparer le rationnel et l'irrationel; l'intellectuellement sain du pathologique; et du lanceur d'alerte du paranoïaque.

### Une première définition
* S'inspire de Rob Brotherton, qui a écrit le livre "*Suspicious Minds*". 
* Il a trouvé 6 caractéristiques qui forment la théorie complotiste prototypique.
    * Une théorie du complot pars toujours de quelque chose qui fait débat ou qu'ils contestent. À partir du moment où un complot est prouvé scientifiquement la "théorie" disparraît. (Questions irrésolues)
    * Pour les complotistes il y a 2 "réalités", l'officielle et illusoire, et la réalité invisible et officieuse. "On nous fait croire que le monde est négatif" (Apparences trompeuses), cela vient de deux auteurs: *Karen Douglas* et *Mike Wood*. *Luc Boltancski*".
    * Une théorie du complot va toujours sur-estimer les capacités de les *comploteurs*. Que tout est controllé et que ces êtres sur-humains sont tout-puissant et ne font pas d'erreurs. (Tout est sous contrôle).
    * Elle dépeint les conspirateurs comme mal intentionnés (Mal incarné)
    * Elle se fonde sur la recherche d'anomalies. La théorie du complot va rechercher les données que le compte-rendu officiel n'arrive pas à expliquer. (recherche d'anomalies)
    * On ne peut pas prouver l'inexistance de quelque chose, il est donc beaucoup plus facile de dire qu'un complot existe, que de prouver qu'il n'existe pas. Également toute "preuve" à l'encontre de la théorie va devenir une preuve de son exitence pour un complotiste (backfire effect) (irréfutabilité)

### Complotiste, de qui parle-t-on ?
* Dire qu'un complotiste est « un adepte d'une théorie du complot » serait assez simpliste et nierais la complexité du phnéomène.
* Barkun, politologue, différencie 3 degrés d'ahésion aux théories du complot
    * **Le conspirationnisme d'évènement**, **un** évènement isolé comme résultat d'un complot
    * **Le consiprationnisme systèmique**, **plusieurs** évènements considérés comme étant le résultat d'un vaste complot à plus **long terme** imputé à une **communauté**. Exemple : les juifs, la mafia, etc.
    * **Le super-conspirationnisme**, **touts** les évènements résulterais d'un **vaste complot global** voire cosmique depuis la **nuit des temps** par une **puissance divine**.

Les étapes de l'adhésion : 

* **L'adhésion épisodique** à l'une ou l'autre théorie, on parle alors de complotisme **inductif** : « Sur base des éléments que j'ai, la théorie du complot me parait l'explication la plus probable »
* On parle de **complotiste** lorsqu'on entre dans le complotisme **déductif** : « Tout nouvel évènement doit être reformulé pour se conformer à la visio ndu monde selon la quelle les complots mènent l'histoire », cela s'appel le **phénomène de révisionnisme instantané (Latour, 2004).

### Un phénomène en augmentation ?
* Malgrès le sentiment partagé d'une augmentation du complotisme, le complotisme n'aurrait en vérité pas augmenté depuis des décennies.
    * Basé sur une recherche aux USA sur l'analyse de lettres envoyées au New York Times depuis 100 ans. 
    * Cela a été fait sans tenir compte que'aujourd'hui les complotistes diffusent plutôt leur contenu sur le Web, ce quit leur donne une meilleur visibilité. 
* Il est difficile d'objectiver ce sentiment pour plusieurs raisons
    * Le complotisme dépend de la mode, elle change en fonction des évènements.
    * Que veux dire précisément « il y a une augmentation du complotisme » ?
        * Augmentation du nombre de récits complotistes ?
        * Augmentation de l'adhésion à certains récits particuliers ?
        * Augmentation de l'explosition de tout un chacun aux thèses complotistes ?
* Ce qui semblerait être la cause de ce sentiment serait un *effet d'optique* lié aux transformations des modes de productions et de diffusion.
    * L'arrivée des réseaux sociaux permet aux théories du complot de rivaliser avec les savoirs "socialement acceptés".
    * La recherche montre en effet qu'il y a un fort lien entre l'exposition et l'adhésion.
* L'auteur choisit d'accpeter sans preuve ce sentiment d'augmentation mais voudrait qu'il y ait plus de recherches à ce sujet.

### Une histoire moderne ?
* Le complotisme a toujours existé, mais la première étude sur le sujet n'a été faite qu'en 1945.
* On peut distinguer le complotisme ancien et moderne (Cubitt, 1989)
    * La date clé serait 1789 ? (pourquoi)
    * Avant 1789, la portée était limitées, après, la portée est étendue.
    * Les comploteurs "anciens" sont des personnages visibles et puissants qui ont pour but d'augmenter leur pouvoir, tandis que les comploteurs "modernes" sont des sociétés secrètes aux compositions et méthode secrètes avec des buts obscures.

### L'émergence de Consipracy Studies
* Boltanski (2012) à dit qu'il y a 5 grants types de travaux sur le complotisme :
    * Dénonciation des différentes thèses complotistes
    * Sur la manière dont les thèses complotistes ont imprgné les oeuvres de fiction.
    * Sur les descriptions de la culture complotiste
    * Sur l'histoire du complotisme et déceler les tendences psychologiques humaines
    * La déconstruction de la notion de complotisme
* Les définitions du complotisme ne sont pas les même en fonctions des discipline.

### Les sciences sociales comme complot
* Popper en 1945 et Latour en 2004 ont rapproché sociologie et complot en parlant même de « théories sociologiques du complot ».
    * La sociologie montre que les instituions sociales telles que la domination du genre, de classe ou d'ethnie n'est pas neutre. disait Bourdieu.
        * Pour Szoc la théorie de la reproduction sociale de Bourdieu n'est pas une théorie du complot car elle ne se fonde par sur des intentions malveillantes.
            * « Complotisme sans intention »

## Psychologie sociale et cognitive : un apport sous-estimé
* La littérature francophone tend à définir le complotisme comme une maladie, et les complotistes comme des malades (ou victimes).
    * En faisant ça, elle analyse et stigmatise en même temps. 
    * Selon Szoc, c'est la raison pour laquelle la lutte contre le complotisme est un échec.
        * Szoc montre donc comment l'adhésion au complotisme peut découler de 4 mécanismes cognitifs humains et normaux.

### Le biais d'intentionalité
* La tendance à expliquer des év§nements comme fruits d'actions intentionnelles
    * Exemple avec l'expérience de Simmel et Heider en 1943 avec les formes géométriques, où presque tous les participants ont assignés des traits humains aux formes géométriques.

### Le biais de confirmation
* Tendance à rejeter tout nouvel élément qui remettrait en question notre représentation du monde (dissonance cognitive)
* À ce jour, internet propose une multitude d'explications pour chaque évènement. Chacun y trouve la confirmation de son hypothèse de départ, évitant ainsi toute dissonance cognitive.

### Le biais de proportionnalité
* Tendance à supposer que les grands événements ont de grandes causes.
    * Trouver que rien est dû au hasard
        * Par exemple, on va lancer un dé "plus fort" quand on a pour objectif d'avoir 6 que qu'en on a pour objectif d'avoir 1, alors même que la probabilité ne change pas.

### Erreur de composition
* Tendance à préférer des éléments qui renforcent la cohérence d'une situation même si ils en réduisent objectivement la probabilité.
    * Voir expérience de Kahneman et Tverski en 1983 : l'histoire de Linda

### Quelles conséquences ?
* Szoc trouve que les études s'intéressent plus souvent aux causes qu'aux conséquences
    * Les conséquences sont souvent supposées et non réellement prouvées. 
        * Exemple : Le désengagement du politique, le replis sur la sphére privée, la démobilisation, le cynisme, les formes d'engagement violents, etc.
* La question de si oui ou non l'adhésion au complotisme influence le débat politique est plus difficile a étudier
    * Szoc croit quand même à la facilitation du démagogisme par le complotisme.

### Un peu d'espoir, le paradoxe de la diffusion
* Quand un petit groupe d'« initiés » connaissent la « vérité », cela crée un sentiment de fierté.
* La diffusion de cette « vérité » au plus grand nombre augmente accentue ce sentiment de fierté mais crée aussi un sentiment de dépit.
    * Dépité  de voir leurs théories dépréciées et discréditées.
* Se différencier du commun des mortels explique partiellement l'adhésion au complotisme.
    * Ainsi, la diffusion du complotisme porte en elle les germes de son échec.
        * Cette phrase est tout de même à nuancer car un phénomène peut avoir plusieurs explications complotistes concurrentes.

## Les réseaux sociaux ou quand le Lord maire devient plus têtu que le fait...
* Les biais cognitifs sont susceptibles d'être amplifié dans un contexte où les réseaux sociaux sont le monde d'accès de plus en plus privilégié à l'information.

### Bulles de filtrage
* Enfermement cognitif / biais idéologique de l'internaute résultant des algorithmes qui sélectionnent pour lui les informations auquelle il aura accès prioritairement.
    * Sélection faite sur base d'intelligence artificielle et des préférences de l'utilisateur.
    * L'internaute n'est confronté uniquement à des opinions et informations pour lesquelles il a manifesté de l'intérêt au préalable. 
        * Cercle vicieux cd confirmation des opinions et d'évitement de la dissonance cognitive.

### Marchandisation
* Principe selon lequel : si l'information est une marchandise, le citoyen devenu consommateur est libre de choisir le produit qui lui convient (et qui ainsi évite la dissonance cognitive)
    * Ce principe est ancien mais les moyens technologiques d'ajourd'hui pour sa mise à son service sont nouveaux.
        * Avant les journeaux papiers n'avaient que peu d'information pour mesurer l'impacte des articles sur ses lecteurs. Tandis qu'aujourd'hui cet impact peut être mesurer avec beaucoup plus de précision : nombres de vues, nombres de commentaires, nombres de clics, temps de lecture, nombre de partage, etc.
            * La vérité ne serait ainsi plus qu'un élément comme les autres qui valorise un article (Viner, rédactrice en chef de « The Guardian », 2016).
                * Bien que Viner ne traite pas de complotisme, Szoc y voit tout de même un fait inquietant où la préférence idéologique des lecteurs jouent un rôle important dans la production d'information.

### Après la vérité
* Internet et les réeaux sociaux favorise les théories du complot au niveau de la diffusion.
* Depuis 2016, de nombreux articles de presse ont fait écho à une inquietude quand à l'arrivée des « post-true politics » (Roberts, 2010)
    * Pour Hotchschild (2016), ce fait n'est pas nouveau mais est une résurgence de partiques médiatiques des 18 et 19e siècles.
    * Ces explications alternatives des évènements suivent un schéma connu : la recherche partialle d'informations pouvant accréditer la thèse.

### Une question de confiance
* De nombreuses enquêtes montrent que la confiance dans les institutions s'est éffondrée (du moins dans les pays industrialisés)
    * Wilkinson et Pickett (2009) établissent un lien entre les inégalités et la confiance dans les institutions.
        * La majeure partie de ce que nous croyons et savons viens des experiences personnellent et pas des institutions commes les écoles.
* Dans un monde ou la confiance dans les institutions est forte, la nécessité pour ces dites institutions de prouver ce qu'elle disent est faible; tandis que dans le cas inverse, la preuve n'est suffisant pour obtenir de la crédibilité.

### Le retour de la propagande
* Si la « propagande d'état » n'a pas disparut avec la destruction du mur de berlin, elle en est devenue quasi synonyme de « propagande américaine ».
    * Pour Szoc ces « lanceurs d'alerte » doivent élargir leur propos et analyser les mécanismes de la propagande russe sans croire que cela servirait d'office les américains.
        * Ceux qui luttent contre l'impérialismes américain l'emplifie, en voyant des traces de celui-ci partout.
* Les analyses du complotisme doivent prendre en compte cette nouvelle répartition géopolitique de la production des récits.

## Complotisme et racisme
* Les liens entre les deux sont ambiguts car l'un peut exister sans l'autre.
    * Cependant le comploteur est souvent racisé selon Szoc.

### Antisémitisme
* De nombreux complots possèdent un noyeau antisémite.
    * La présence de Juifs sur un territoire donné n'influence en rien la théorie du complot. Les cehrcheurs parlent alors « d'antisémitisme sans juifs »
* Taguieff (2016) définit les *4 étapes du complot juif*
    * Pendant l'antiquité ont repprochaient aux juifs leur solidarité et leurs exclusivité. On les accuse de meurtres rituels chez des enfants chrétiens au 12e siècle.
    * Au 19e siècle, on repproche aux juifs de vouloir domminer financièrement le monde.
    * Au 20e siècle, on repproche aux juifs de vouloir domminer le monde en propageant de la démocratie individualiste sapant les bases de la société.
    * 1948, création d'Israel, le mythe connais son dernier avatar.

### Eurabia ou « l'invasion arabe »
* La thèse d'origine de l'invasion arabe est : *« L'europe est en voie d'arabisation à cause de l'immigration massive, différentiels de taux de fécondités, trahisons des élites européennes, etC.*
* L'extrème droite s'est saisie de ce complot pour se justifier.
* A ce jour le complot est devenu un dangereux mythe islamophobe alors même qu'il contient une série d'invraisemblance.

## Conclusion : le complotisme comme symptome
* Si le complotismme est un symptôme apparent du dysfonctionnement démocratique global, il est donc inutile de s'attaquer à ce symptome
* Mais en même temps
    * Il faut donc voir le complotisme à la fois comme un symptome ET à la fois comme une maladie.
* La lutte contre le complotisme doit donc se mener dans le champ de la politique et non dans la « contre propagande »
