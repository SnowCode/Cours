## Fiche d'identité du livre
| --- | --- |
| --- | --- |
| Nom de l'auteur | Camus |
| Date de vie et de mort | 7 novembre 1913 - 4 janvier 1960 |
| Fonctions | Écrivain, philosophe, romancier. A reçu un prix nobel de littérature. |
| Nom du livre | Les Justes |
| Signification  |  |
| Date du livre | Début du 20e siècle |
| Courant littéraire | Existentialisme |
| Genre de l'oeuvre | Pièce de théatre |
| Thèmes de l'histoire | Justice, Equité |

## Liste des personages principaux
| Personnage | Actions |
| ---------- | ------- |
| Dora | |
| La grande duchesse | 
| Kaliayev | |
| Stepan | | 

## Résumé par chapitre 

## Synthèse du courant 

## Lien entre le livre et le courrant
