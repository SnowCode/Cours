## Fiche d'identité du livre
| --- | --- |
| --- | --- |
| Nom de l'auteur | Charles Baudelaire |
| Date de vie et de mort | 9 avril 1821 - 31 aout 1867 |
| Fonctions |  |
| Fait marquants | Constamment endetté, pressé de fuilt il occupera 40 domiciles |
| Nom du livre    | Les fleurs du mal |
| Signification  | Métaphore qui montre que l'on peut cultiver le bien dans le mal |
| Date du livre | Milieu 19e siècle |
| Courant littéraire | Modernité littéraire |
| Genre de l'oeuvre | Receuil de poésie |
| Thèmes de l'histoire | Bien, mal, nouveauté et passion |

## Liste des personages principaux
Lui même

## Résumé par chapitre 
Montre la fraternité des hommes devant le mal.

## Synthèse du courrant

## Lien entre le livre et le courrant
