## Fiche d'identité du livre
| --- | --- |
| --- | --- |
| Nom de l'auteur | Voltaire, de son vrai nom *François-Marie Arouet* |
| Date de vie et de mort | 21 Novembre 1694 – 30 Mai 1778 |
| Fonctions | Historiographe de France, Fauteuil 33 de l'Académie française |
| Nom du livre    | Candide (ou l'optimiste) |
| Signification  | C'est le nom d'un personnage |
| Date du livre | Paru en 1759 |
| Courant littéraire | Les lumières |
| Genre de l'oeuvre | Conte philosophique |
| Thèmes de l'histoire | La désillusion et l'optimisme |

## Liste des personages principaux
| Nom | Ce qu'il fait |
| --- | ------- | 
| Candide | Un jeune homme bâtard, il peut se résumé à « Tous est pour le mieux dans le meilleur des mondes possibles », amoureux de Cunégonde |
| Cunégonde | 


## Résumé par chapitre 

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/15-2ogxMWKo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

| Chapitre | Ce qu'il s'y passe | 
| --- | --- |
| 1 | **Candide** est surpris en train d'embrasser **Cunégonde** et se fait chasser |
| 2-3 | **Candide** est enrôlé de force comme soldat dans l'armée bulgare et assiste à une guerre effroyable que Voltaire décrit comme une « boucherie héroïque », **Candide** fuis alors en Hollande et alors que personne ne veut lui donner à manger il est recueilli par **Jacques**. |
| 4-5 | **Candide** retrouve **Pangloss** défiguré par la variole. Il lui apprends que le château a été détruit, le baron, la baronne et **Cungonde** tués. Il est également receilli par **Jacques** qui le soigne. Puis les trois hommes prennent un bateau pour le Portugal. Sauf qu'une tempête détruit l'embracation et **Jacques** se noie. **Candide** et **Pangloss** sont arrêtés par l'inquisition, l'un pour avoir parlé, et l'autre pour avoir écouté. |
| 6-7 | Un auto-dafé à lui pour empécher la terre de trembler à nouveau. **Pangloss** est pendu et **Candide** est fessé. **Candide** rencontre une vieille femme qui le  soigne et le remet en contact avec **Cunégonde** |
| 8-10 | **Cunégonde** exlpique que les bulgares ont détruit le chateau, tué ses parents, l'ont violé et éventré. Mais fut recueillie et soignée par un bulgare. Mais l'homme la vendit à un juif nommé Don Issachar. **Candide** tue Don Issachar et l'Inquisiteur. Il s'enfuit pour l'Amérique avec **Cunégonde** et la vieille femme. |
| 11-12 | La vieille femme raconte son histoire, elle vécut dans le luxe avant de se faire attaquer par des Corsaires et de se faire vendre. |
| 13 | Ils arrivent à Buenos Aires où le gouverneur s'eperneds de passion pour **Cunégonde**. **Candide** doit fuire et laisser **Cunégonde** là sous les conseils de la vieille femme. |
| 14-15 | **Candide** retrouve le frère de **Cunégonde**, **Candide** lui explique que **Cunégonde** est vivante et qu'il veut se marrier avec elle. Le frère s'oppose au mariage et **Candide** le tue et s'enfuit avec son valet (**Cacambo**). |
| 16-18 | **Cacambo** et **Candide** évite de peu d'être mangé par des sauvages et parviennent au pays de l'eldorado. Ils décident tout de me de quitter ce pays merveilleurs avec les moutons chargés d'or  pour rachter **Cunégonde** au gouverneur |
| 19-20 | Ils rencontrent un nègre, il a une main et une jambe coupée. Candide pleure et commence à remettre en question l'optimisme prôné par son maître **Pangloss**. Il se fait voler son or et embarque por l'Europe avec **Martin**, un philosophe. **Martin** est pessimiste et pense que tout va mal. |
| 21-23 | A Paris, **Candide** se fait voler, il fuit vers l'Angleterre où il assiste à une exécution publique. Ils partent pour Venise. |
| 24-25 | Ils rencontrent **Paquette** qui était une servante pour **Cunégonde** et amante de **Pangloss**. Elle est aujourd'hui prosituée. |
| 26-29 | **Candide** retrouve **Cacambo** et apprend que **Cunégonde** est en esclavage en Turquie. Il embarque pour Constantinople et retrouve **Pangloss** et de frère de Cunégonde qui tout deux lui explique comment ils ont survécus. Ils retrouvent **Cunégonde**, devenue laide, **Candide** renouvelle sa demande, le frère s'y oppose et est renvoyé aux galères. | 
| 30 | Le groupe s’installe dans une petite maison. **Pangloss**, **Martin** et **Candide** s’entretiennent avec un vieux turc qui leur explique que son bonheur vient du fruit du travail de ses terres. **Candide** prend du recul par rapport à la philosophie optimiste et comprend qu’il faut goûter au bonheur simple en apprenant à « cultiver son jardin ». |


## Synthèse du courrant des lumières
Les Lumières, ce sont les idées nouvelles diffusées par les philosophes qui croient au progrès humain et sont guidés par la raison (et non par la tradition et l’autorité).

### Contexte historique
Le siècle des Lumières est initié par deux événements historiques fondateurs : la révolution d’Angleterre en 1688-1689 et la mort, en 1715, du monarque absolu de droit divin Louis XIV, qui laisse place à un mouvement de contestation de l’ordre établi.

Mais il y a également l'essort des sciences et le développement du livre qui entre en jeu.

### Principes (caractéristiques)
* La raison et le combat des préjugés 
* Contestation sociale et politique
* Foi dans la science et dans le progrès
* Valorisation des sentiments
* Recherche du bonheur

## Lien entre le livre et le courrant
* 
