## Fiche d'identité du livre
| --- | --- |
| --- | --- |
| Nom de l'auteur | Gustave Flaubert |
| Date de vie et de mort | décembre 1821 - mai 1880 |
| Fonctions | Écrivain, a été attaqué en justice à cause de son livre. |
| Nom du livre | Madame de Bovary |
| Signification | C'est un personnage du livre. |
| Date du livre | Milieux du 19e siècle |
| Courant littéraire | Réalisme |
| Genre de l'oeuvre | Roman |
| Thèmes de l'histoire | Routine, richesse, frustration |

## Liste des personages principaux

## Résumé par chapitre 

## Synthèse du courrant 

## Lien entre le livre et le courrant
