## Fiche d'identité du livre

| --- | --- |
| --- | --- |
| Nom de l'auteur | Jean Racine |
| Date de vie et de mort | 1639 - 1699 |
| Fonctions et fait marquant | Fauteuil 13 de l'Académie française, historiographe du roi |
| Nom du livre    | Phèdre |
| Signification  | C'est le nom d'un personnage |
| Date du livre | 1677 |
| Courant littéraire | Le classicisme |
| Genre de l'oeuvre | Classique |
| Thèmes de l'histoire | L'amour, la culpabilité et la passion |

## Liste des personages principaux
| Nom | Ce qu'il fait |
| --- | --- |
| Phèdre | Seconde femme de Thésée |
| Oenone | Confidente de Phèdre | 
| Hyppolyte | Fils de Thésée et d'une Amazone (beau-fils de Phèdre) | 
| Théramène | Gouverneur d'Hyppolyte |
| Thésée | Roi d'Athènes |
| Panope | Messager qui annonce la mort de Thésée |
| Aricie | Ancienne princesse rescapée d'un massacre, est en otage depuis |
| Ismène | Confidente d'Aricie | 

## Résumé par chapitre 
| Scène | Ce qu'il s'y passe |
| --- | --- |
| A1S1 | **Hippolyte** annonce à **Théramène** qu'il veut partir de Trézène pour chercher **Thésée**. Il veut aussi fuir **Aricie** car il pense qu'elle le hait. | 
| A1S2 | **Phèdre** est annoncée |
| A1S3 | **Phèdre** est triste. **Oenone** lui dit qu'elle se laisse mourir. **Phèdre** lui avoue qu'elle aime **Hippolyte** elle trouve que sa seule solution est de se laisser mourrir pour ne pas commettre l'inceste |
| A1S4 | **Panope** annonce la mort de **Thésée** |
| A1S5 | La mort du roi donne lui àun enjeu politique. C'est soit les enfants de **Phèdre** ou **Hyppolyte** qui va sur le throne. **Oenone** conseille à **Phèdre** de défendre les droits de sess enfants avant Hyppolyte.  |
| A2S1 | **Aricie** avoue à **Ismène** qu'elle aime Hyppolyte. **Hyppolye** est nomé roi par la ville de Trézène. |
| A2S2 | **Hyppolye** rends sa liberté à **Aricie** et lui propose de l'épouser. Il propose un partage de terre pour que personne ne se fasse assaciner | 
| A2S3-4 | **Phèdre** veut voir **Hippolyte**. **Aricie** accepte la demande d'**Hippolyte**. **Hippolyte** veut partir mais ne peut pas car **Phèdre** arrive. | 
| A2S5 | **Phèdre** avoue à **Hippolyte** qu'elle l'aime. **Hippolyte** est choqué et la repousse violemment. **Phèdre** lui arrache son épée et le menace de se tuer mais **Oenone** l'entraine hors de la scène. |
| A2S6 | Le fils de **Phèdre** est reconnu comme roi par Athènes mais le bruit cours que **Thésée** n'est pas mort. |
| A3S1-2 | **Phèdre** ne veut pas régner. Elle veut offrir le throne à **Hippolye** pour gagner son amour. Elle en est accablée de honte et implore Vénus pour l'aider. |
| A3S3 | **Thésée** est vivant. Pour éviter la honte **Phèdre** veut se tuer mais **Oenone** lui propose de faire exiler **Hippolyte** par son père en lui accusant de faire des avances à Phèdre. | 
| A3S4 | **Phèdre** est très mal à l’aise quand son mari arrive et **Thésée** est méfiant. | 
| A3S5 | **Hippolyte** annonce à son père qu’il quitte Trézène, ce qui rend **Thésée** encore plus méfiant. | 
| A3S6 | **Hippolyte** est inquiet des révélations que Phèdre pourrait faire. Il craint de perdre l’amour de **Thésée**. | 
| A4S1 | **Oenone** dit à Thésée qu'**Hyppolyte** a essayé de séduire **Phèdre**. Furieux, **Thésée** demande à Neptune de tuer son fils. | 
| A4S2-3 | **Thésée** décide de faire avouer sa faute à **Hippolyte**, mais ce dernier se défends en disant qu'il aime **Aricie**. Le roi bannis son fils. |
| A4S4-5 | **Phèdre** apprends qu'**Hippolye** aime **Aricie**. Elle est fole de douleur. | 
| A4S6 | **Phèdre** veut denouveau mourrir à cause de la jalousie. **Phèdre** maudit **Oenone** |
| A5S1-2 | **Hippolyte** s'enfuit suivi par **Aricie**, ils se fiancent devant les dieux. |  
| A5S3 | **Thésée** a vu **Aricie** et **Hippolyte** ensemble; il commence à douter. | 
| A5S4 | Ses doutes se confirmes et veut voir **Oenone** | 
| A5S5 | **Oenone** s'est noyée. **Phèdre** s'apprête à se suicider. **Thésée** comprends et rappel **Hippolyte** | 
| A5S6 | **Théramène** annonce la mort d'**Hippolyte** | 
| A5S7 | **Phèdre** avoue son crime avant de mourrir. **Thésée** décide d'adopter **Aricie** |

## Synthèse du courrant des classiques
Le classicisme est un mouvement littéraire du 17ème siècle et qui atteint son appogée avec le reigne de Louis XIV (roi soleil).

Le classicisme se caractérise par la recherche de l’ordre, de la clarté, de la mesure et de la retenue. Loin du foisonnement baroque, l’écriture classique est maîtrisée et se plie à des règles exigeantes.

### Contexte historique
Le mouvement baroque reflétait l'instabilité du monde, notament à cause des guerres de religions.

Or, pendant le classicisme, la France est dans une période de stabilité religieuse (catholicisme), politique et économique (centralisation et art).

Le classicisme reflète cette stabilité qui prédomine sous le reigne de Louis XIV.

### Caractéristiques
Dans le but d'imiter la Nature et les Anciens (latins et grecs) et ainsi se conformer à la raison, ils se fixent des règles à respecter.

Voici ces règles:

* Règle de vraisemblance, ce qui est raconté ou représenté doit être vraisemblable.
* Les règles des trois unités (temps, lieu et action)
* La règle de bienséance, ne pas montrer ce qui pourrait choquer le bon goût)
* Volonté de plaire et instruire, il y a une utilité morale aux oeuvres classiques (catharcis, purgation des passions)
* Un idéal, du parfait homme du XVIIe siècle.

## Lien entre le livre et le courrant
Racine représente l'idéal de la tragédie classique, voici les caractéristiques :

* Il n'y a aucun élément surnaturel dans Phèdre
* Il n'y a qu'une seule unité de temps, un seul lieu et une seule action.
* Il n'y a jamais la mort d'un personage montrée sur scène, ni rien de la sorte.
* Phèdre enseigne sur le danger des passions (nottament la jalousie, l'amour, l'inceste, etc)
* Phèdre est l'inverse de la personne idéale au XVIIe siècle.
